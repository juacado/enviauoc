EnviaUOC is an Android app assignment for Open University of Catalonia (Universitat Oberta de Catalunya - UOC).

It is about implementing a mobile app for a courier company with which users can:

- sign in the system,
- know delivery status,
- define delivery address using a map.

This Android app is developed using Eclipse and Android Development Tools.

In order to make it work you might need to setup v7 appcompat library as explained in the following link:

https://developer.android.com/tools/support-library/setup.html